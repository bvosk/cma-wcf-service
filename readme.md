# Project 3: Code Maintainability Analyzer

## Overview

This solution fulfills most of the requirements of project 3. All projects have
been created from scratch except for the basis for the WCF service, which were copied from the `BasicHttpServices` solution provided. These projects are:

- `IRemoteRepositoryService` - Defines the WCF service contract
- `RemoteRepositoryService` - Implements the WCF service contract
- `ServiceClient` - Implements the WCF client
- `ServiceHost` - Implements the WCF host

The following projects have been added:

- `RemoteRepository` - A [data-tier application](https://docs.microsoft.com/en-us/sql/relational-databases/data-tier-applications/data-tier-applications) which defines the necessary database schema
- `LocalRepository` - A project which manages access to the databse through a common interface. This is intended to reflect the [repository pattern](https://msdn.microsoft.com/en-us/library/ff649690.aspx)
- `Common` - Used to share domain objects between projects
- `CmaGui` - Implements a simple WPF GUI for a Remote Repository client.

## Requirement Compliance

- ✔ Shall be written in C#, using the .Net Framework. You may also use helper code provided in Project3Helpers.

- ✔ Shall use Visual Studio 2015, Community Edition available at no cost.

- ⚠ Shall implement a message-passing communication channel using Windows Communication Foundation (WCF).

- ⚠ Shall define a Message class that provides:
  - Source and Destination addresses
  - Message type
  - Author
  - Time-Date
  - String body:
  - Expected to hold an XML string supplying information needed to execute a specific request.
  
  > This project uses RPCs rather than message passing in the WCF interface. The `PluggableComm` project, which demonstrates the use of message passing, was discovered too late in the development of this project. It proved infeasible to switch from an RPC to a message passing architecture in the allotted time.

- ✔ Shall define service contracts for sending messages and for uploading and downloading files.

- ✔ Shall provide simple test client and server, designed to show that the communication channel works as specified.

  > The `Main` method in the `ProgClient.cs` file defined in the `ServiceClient` project broadly tests the functionality of the implemented communication channel. The project must be changed to a console application to run this test.

- ✔ Shall provide a Graphical User Interface with a set of tabbed views. Each view provides or displays information for one of the Repository activities, e.g., CheckIn, CheckOut, Browsing, and making queries about the stored package content.

- ✔ Shall provide, for each view, one or more packages that perform all of the processing necessary to send requests, receive query information, upload and download files.

- ❌ Shall use the Local Code Analyzer of Project #2 to test the GUI operations2.

  > There was not enough time to add the local code analyzer in project #2 into the remote repository. This would be a straightforward process with enough time to implement the associated GUI.