﻿/////////////////////////////////////////////////////////////////////////
// MainWindow.xaml.cs                                                  //
//                                                                     //
//                                                                     //
//                                                                     //
// Brian Voskerijian, CSE681-Software Modeling & Analysis, Spring 2018 //
/////////////////////////////////////////////////////////////////////////
//
// - Implements the main window GUI for the CMA
/////////////////////////////////////////////////////////////////////////

using Common;
using ServiceClient;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;
using Path = System.IO.Path;

namespace CmaGui
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public ProgClient _Client { get; set; }
    private User _LoggedInUser { get; set; }
    private IEnumerable<string> _Projects { get; set; }

    // Encapsulate LoggedInUser to simplify logic behind logging in/out
    public User LoggedInUser {
      get { return _LoggedInUser; }
      set
      {
        _LoggedInUser = value;
        if (value != null)
        {
          // User is logged in, don't allow logging in again and show logged in username
          LogoutButton.Visibility = Visibility.Visible;
          LoggedInLabel.Content = $"Logged in as [{value.Username}]";
          LoginButton.IsEnabled = false;
          RegisterButton.IsEnabled = false;
          RefreshProjectList(); // Get projects for this user
        }
        else
        {
          // User is logged out, so allow logging in again
          LogoutButton.Visibility = Visibility.Hidden;
          LoggedInLabel.Content = string.Empty;
          LoginButton.IsEnabled = true;
          RegisterButton.IsEnabled = true;
        }
      }
    }

    public MainWindow()
    {
      InitializeComponent();

      // Initialize remote repository client
      string url = "http://localhost:8080/RemoteRepositoryService"; // Must match URL specified in host
      _Client = new ProgClient(url);
    }

    private void LoginButton_Click(object sender, RoutedEventArgs e)
    {
      var usernameText = UsernameTextBox.Text;
      var user = _Client.GetUser(usernameText);

      // Check if this user exists
      if (user == null)
      {
        MessageBox.Show("No user matching that username was found.", "No user found", MessageBoxButton.OK);
      }
      LoggedInUser = user;
    }

    private void RegisterButton_Click(object sender, RoutedEventArgs e)
    {
      var usernameText = UsernameTextBox.Text;
      var user = _Client.GetUser(usernameText);

      if (user == null)
      {
        // User does not exist, create a new one and log them in
        _Client.CreateUser("Developer", usernameText);
        user = _Client.GetUser(usernameText);
        MessageBox.Show($"Registered and logged in as new user [{usernameText}]", "New User Created", MessageBoxButton.OK);
        LoggedInUser = user;
      }
      else
      {
        // User does exist, deny registration
        MessageBox.Show($"Cannot register this username because a user with this name already exists.", "User Already Exists", MessageBoxButton.OK);
      }
    }

    private void LogoutButton_Click(object sender, RoutedEventArgs e)
    {
      LoggedInUser = null;
    }

    private void NewProjectButton_Click(object sender, RoutedEventArgs e)
    {
      var projectNameDialog = new ProjectNameDialog();
      projectNameDialog.ShowDialog();
      if (projectNameDialog.ProjectName != null)
      {
        // Project name was entered and dialog was not closed out, so create the project
        _Client.CreateProject(LoggedInUser.Username, projectNameDialog.ProjectName);
        RefreshProjectList();
      }
    }

    private void RefreshProjectList()
    {
      // Get projects from client
      ProjectListBox.ItemsSource = _Client.BrowseProjects(_LoggedInUser.Username);

      // Set listbox to first project in the list
      if (ProjectListBox.Items.Count > 0)
      {
        ProjectListBox.SelectedIndex = 0;
      }
    }

    private void ProjectListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      RefreshFileList();
    }

    private void RefreshFileList()
    {
      var selectedProject = ProjectListBox.SelectedItems[0] as string;
      if (ProjectListBox.SelectedItems[0] != null)
      {
        // Update files
        FileListBox.ItemsSource = _Client.BrowseFiles(LoggedInUser.Username, selectedProject);
      }
    }

    private void CheckInButton_Click(object sender, RoutedEventArgs e)
    {
      // Esure a project is selected
      if (ProjectListBox.SelectedItems.Count == 0)
      {
        return;
      }

      // Get selected project name
      var selectedProjectName = ProjectListBox.SelectedItems[0] as string;

      // Open a file dialog
      var fileDialog = new OpenFileDialog
      {
        Filter = "C# Files|*.cs" // Only allow C# files to be checked in
      };
      var dialogResult = fileDialog.ShowDialog();

      // Check in selected files (may be more than one)
      if (dialogResult.HasValue && dialogResult.Value)
      {
        var selectedFiles = fileDialog.OpenFiles();
        var filenames = fileDialog.FileNames;

        for (var i = 0; i < selectedFiles.Length; i++)
        {
          var filename = Path.GetFileName(filenames[i]);
          _Client.CheckIn(_LoggedInUser.Username, selectedProjectName, filename, selectedFiles[i]);
        }
      }

      /* Wait for check in to complete before refreshing the file list
       Not good to block UI thread, but this is makes the UI behave more predictable to the user (new file shows up automatically).
       The delay is very short so it is not noticeable to the user. */ 
      Thread.Sleep(100);
      RefreshFileList();
    }

    private void CheckOutButton_Click(object sender, RoutedEventArgs e)
    {
      // Esure a file is selected
      if (FileListBox.SelectedItems.Count == 0)
      {
        return;
      }

      // Get selected project/file name
      var selectedFile = FileListBox.SelectedItems[0] as CmaFile;

      // Open a save as dialog
      var saveAsDialog = new SaveFileDialog
      {
        Filter = "C# Files|*.cs",
        FileName = selectedFile?.Name ?? throw new InvalidOperationException(),
        DefaultExt = ".cs",
        
      };
      var dialogResult = saveAsDialog.ShowDialog();

      // Save file (may be more than one)
      if (dialogResult.HasValue && dialogResult.Value)
      {
        using (var hostFile = _Client.CheckOut(_LoggedInUser.Username, selectedFile?.Name, selectedFile?.ProjectName))
        using (var clientFile = saveAsDialog.OpenFile())
        {
          hostFile.CopyTo(clientFile);
        }
      }
    }
  }
}
