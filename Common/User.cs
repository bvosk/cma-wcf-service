﻿/////////////////////////////////////////////////////////////////////////
// User.cs                                                             //
//                                                                     //
//                                                                     //
//                                                                     //
// Brian Voskerijian, CSE681-Software Modeling & Analysis, Spring 2018 //
/////////////////////////////////////////////////////////////////////////
//
// - Implements a domain object for CMA users
/////////////////////////////////////////////////////////////////////////
namespace Common
{
  public class User
  {
    public int Id { get; set; }
    public string Role { get; set; }
    public string Username { get; set; }
  }
}
