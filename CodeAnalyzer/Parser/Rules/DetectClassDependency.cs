﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser.Parser;

/*
 * DetectClassDependency
 *
 * This class implements a rule to detect class dependencies. It requires a list of classes to
 * search for in its constructor. This is to be obtained on the first pass of the parser.
 *
 */
namespace Parser.Rules
{
    public class DetectClassDependency : ARule
    {
      private readonly List<Elem> _classes;
      private readonly Repository _repo;

      public DetectClassDependency(Repository repo, List<Elem> classes)
      {
        _classes = classes;
        _repo = repo;
      }

      // Assumption: Assumes only one class dependency per semi-expression
      public override bool test(CSemiExp semi)
      {
        var typeList = new List<string> {"class", "interface", "struct"};

        var currentClassName = _repo.stack.GetContext(e => e.type == "class")?.name;
        if (currentClassName == null)
        {
          // We're not inside a class
          return false;
        }

        var possibleDependencies = _classes
            .Where(e => typeList.Contains(e.type))
            .Where(c => c.name != currentClassName)
            .Select(c => c.name);

        foreach (var dependency in possibleDependencies)
        {
          var tokenIndex = semi.FindFirst(dependency);

          if (tokenIndex != -1)
          {
            var local = new CSemiExp();
            local.Add(currentClassName);
            local.Add(dependency);
            doActions(local);
          }
        }

        // Always return false so this rule doesn't interfere with others
        return false;
      }
    }
}
