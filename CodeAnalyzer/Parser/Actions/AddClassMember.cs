﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser.Parser;

/*
 * AddClassMember
 *
 * This class implements an action to store a variable identified to be part of a class.
 *
 */
namespace Parser.Actions
{
  class AddClassMember : AAction
  {
    private readonly Repository _repo;

    public AddClassMember(Repository repo)
    {
      _repo = repo;
    }

    public override void doAction(CSemiExp semi)
    {
      (var keyClass, var member) = (semi[0], semi[1]);

      if (!_repo.ClassMembers.ContainsKey(keyClass))
      {
        _repo.ClassMembers[keyClass] = new List<string>();
      }

      if (!_repo.ClassMembers[keyClass].Contains(member))
      {
        _repo.ClassMembers[keyClass].Add(member);
      }
    }
  }
}
