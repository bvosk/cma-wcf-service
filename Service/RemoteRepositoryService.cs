﻿/////////////////////////////////////////////////////////////////////////
// Service.cs - Programmatic RemoteRepositoryService demo                         //
//                                                                     //
// Jim Fawcett, CSE681 - Software Modeling and Analysis, Fall 2010     //
/////////////////////////////////////////////////////////////////////////
//
// - Started with C# Class Library Project
// - Made reference to .Net System.ServiceModel
// - Added using System.ServiceModel
// - Made reference to IService dll
/////////////////////////////////////////////////////////////////////////
// This file has been adapted for use by Brian Voskerijian
/////////////////////////////////////////////////////////////////////////

#define SERVICE_PRINT

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using LocalRepository;
using System.Threading;
using Common;

namespace RemoteRepositoryService
{
  /*
   * InstanceContextMode determines the activation policy, e.g.:
   * 
   *   PerCall    - remote object created for each call
   *              - runs on thread dedicated to calling client
   *              - this is default activation policy
   *   PerSession - remote object created in session on first call
   *              - session times out unless called again within timeout period
   *              - runs on thread dedicated to calling client
   *   Singleton  - remote object created in session on first call
   *              - session times out unless called again within timeout period
   *              - runs on one thread so all clients see same instance
   *              - access must be synchronized
   */
  [ServiceBehavior(InstanceContextMode=InstanceContextMode.PerCall, IncludeExceptionDetailInFaults = true)]
  public class RemoteRepositoryService : IRemoteRepositoryService
  {
    private readonly IRepository _repository;

    public RemoteRepositoryService()
    {
      _repository = new FileRepository(@"C:\RemoteRepository");
    }

    public bool CreateUser(string role, string username)
    {
      _repository.AddUser(new User
      {
        Role = role,
        Username = username
      });

      return true;
    }

    public IEnumerable<User> GetUsers()
    {
      return _repository.GetUsers();
    }

    public User GetUser(string username)
    {
      return _repository.GetUser(username);
    }
    public IEnumerable<string> BrowseProjects(string username)
    {
      return _repository.GetProjects(username);
    }

    public IEnumerable<CmaFile> BrowseFiles(string username, string project)
    {
      return _repository.GetFiles(username, project);
    }

    public bool ShareProject(string username, string project)
    {
      throw new NotImplementedException();
    }

    public void CheckIn(CheckInData data)
    {
      using (var ms = new MemoryStream())
      {
        data.File.CopyTo(ms);
        _repository.CheckIn(data.Username, data.Project, data.Filename, ms.ToArray());
      }
    }

    public Stream CheckOut(string username, string filename, string project)
    {
      var file = _repository.CheckOut(username, project, filename);

      if (file == null)
      {
        throw new ArgumentException("The specified file does not exist");
      }

      return new MemoryStream(file.Data);
    }

    public bool CreateProject(string username, string projectName)
    {
      return _repository.CreateProject(username, projectName) > 0;
    }
  }
}
