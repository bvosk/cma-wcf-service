﻿/////////////////////////////////////////////////////////////////////////
// ProgClient.cs - Service Client for Programmatic BasicService demo   //
//   version 2 - added ServiceRetryWrapper                             //
//   Uses BasicHttpBinding                                             //
//                                                                     //
// Jim Fawcett, CSE681 - Software Modeling and Analysis, Fall 2010     //
/////////////////////////////////////////////////////////////////////////
//
// - Started with C# Console Application Project
// - Made reference to .Net System.ServiceModel
// - Added using System.ServiceModel
// - Added code to create communication channel
/////////////////////////////////////////////////////////////////////////
// This file has been adapted for use by Brian Voskerijian
/////////////////////////////////////////////////////////////////////////

using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;
using RemoteRepositoryService;
using System.IO;
using Common;
using System.Collections.Generic;

namespace ServiceClient
{
  public class ProgClient
  {
    IRemoteRepositoryService svc;

    public ProgClient(string url)
    {
      svc = CreateProxy<IRemoteRepositoryService>(url);
    }
    //----< returns a proxy of type T >----------------------------------
    /*
     * Proxy is a local object that transparently uses
     * communication channel to converse with server
     * using contract C.
     * 
     * Channel doesn't attempt to connect to server
     * until first service call.
     */
    static C CreateProxy<C>(string url)
    {
      BasicHttpBinding binding = new BasicHttpBinding();
      EndpointAddress address = new EndpointAddress(url);
      ChannelFactory<C> factory = new ChannelFactory<C>(binding, address);
      return factory.CreateChannel();
    }
    //----< Wrapper attempts to call service method several times >------
    /*
     *  Func<T> is a delegate that invokes functions
     *  which take no arguments and return strings
     */
    T ServiceRetryWrapper<T>(Func<T> fnc)
    {
      int count = 0;
      T returnValue;
      while (true)
      {
        try
        {
          returnValue = fnc.Invoke();
          break;
        }
        catch (Exception exc)
        {
          if (count > 4)
          {
            throw new RetryException("Max retries exceeded"); ;
          }
          Console.Write("\n  {0}", exc.Message);
          Console.Write("\n  service failed {0} times - trying again", ++count);
          Thread.Sleep(100);
        }
      }
      return returnValue;
    }

    public bool CreateUser(string role, string username)
    {
      return ServiceRetryWrapper(() => svc.CreateUser(role, username));
    }

    public void CheckIn(string username, string projectName, string filename, Stream filestream)
    {
      svc.CheckIn(new CheckInData
      {
        Username = username,
        Project = projectName,
        Filename = filename,
        File = filestream
      });
    }

    public Stream CheckOut(string username, string filename, string project = null)
    {
      return ServiceRetryWrapper(() => svc.CheckOut(username, filename, project));
    }

    public bool CreateProject(string username, string projectName)
    {
      return ServiceRetryWrapper(() => svc.CreateProject(username, projectName));
    }

    public User GetUser(string username)
    {
      return svc.GetUser(username);
    }

    public IEnumerable<string> BrowseProjects(string username)
    {
      return svc.BrowseProjects(username);
    }

    public IEnumerable<CmaFile> BrowseFiles(string username, string projectName)
    {
      return svc.BrowseFiles(username, projectName);
    }
  }
}

#if PROG_CLIENT_TEST
    static void Main(string[] args)
    {
      Console.Title = "BasicHttp Client";
      Console.Write("\n  Starting Programmatic Basic Service Client");
      Console.Write("\n ============================================\n");

      string url = "http://localhost:8080/RemoteRepositoryService";
      ProgClient client = new ProgClient(url);

      var username = "Brian";
      Console.WriteLine($"Creating user {username} as a Developer.");
      client.CreateUser("Developer", username);

      username = "Kim";
      Console.WriteLine($"Creating user {username} as a Developer.");
      client.CreateUser("Developer", username);

      username = "Dan";
      Console.WriteLine($"Creating user {username} as a Adminstrator.");
      client.CreateUser("Adminstrator", username);

      var projectName = "TestProject1";
      Console.WriteLine($"Creating project {projectName} for user {username}.");
      client.CreateProject(username, projectName);

      var filepath = @"C:\Client.cs";
      var filename = Path.GetFileName(filepath);
      using (var fs = new FileStream(filepath, FileMode.Open))
      {
        Console.WriteLine($"Checking in file at path {filepath} for user {username} and project {projectName}.");
        client.CheckIn(username, projectName, filename, fs);
      }

      var waitTime = 500;
      Console.WriteLine($"Waiting for {waitTime}");
      Thread.Sleep(waitTime);

      var newFilePath = @"CheckedOut\Client.cs";
      Console.WriteLine($"Checking out file to path {newFilePath} for user {username} and project {projectName}.");
      var newFs = client.CheckOut(username, filename, "TestProject1");
      Directory.CreateDirectory("CheckedOut");
      newFs.CopyTo(new FileStream(newFilePath, FileMode.Create));

      Console.ReadKey(); // Wait for user to quit
    }

#endif