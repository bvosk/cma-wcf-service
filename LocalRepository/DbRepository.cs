﻿/////////////////////////////////////////////////////////////////////////
// Repository.cs                                                       //
//                                                                     //
//                                                                     //
//                                                                     //
// Brian Voskerijian, CSE681-Software Modeling & Analysis, Spring 2018 //
/////////////////////////////////////////////////////////////////////////
//
// - Implements the data access layer for the remote repository
// - All interactions with the database occur through this class
/////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System;
using Common;

namespace LocalRepository
{
  public class DbRepository : IRepository
    {
    private IDbConnection Connection => new SqlConnection(ConnectionString);

    // Just using a local DB here. Can change this to some other database in production
    private static string ConnectionString => @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=RemoteRepository;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

    // Get all cma users
    public IEnumerable<User> GetUsers()
    {
      const string command = "select * from Users order by Username";
      return Connection.Query<User>(command);
    }

    // Get specified cma user
    public User GetUser(string username)
    {
      const string command = "select top 1 * from Users where username = @username order by Username";
      return Connection.Query<User>(command, new { username }).FirstOrDefault();
    }

    // Add a cma user
    public void AddUser(User user)
    {
      const string command = "insert into Users(Role, Username) values(@Role, @Username)";
      Connection.Query<User>(command, 
        new
        {
          user.Role,
          user.Username
        });
    }

    // Clear users (used for testing)
    public void ClearUsers()
    {
      const string command = "delete from Users";
      Connection.Query(command);
    }

    // Clear projects (used for testing)
    public void ClearProjects()
    {
      const string command = "delete from Projects";
      Connection.Query(command);
    }

    // Clear files (used for testing)
    public void ClearFiles()
    {
      const string command = "delete from Files";
      Connection.Query(command);
    }

    // Clear all data (used for testing)
    public void ClearAll()
    {
      ClearUsers();
      ClearProjects();
      ClearFiles();
    }

    // Determine if cma user already exists
    public bool UserExists(string username)
    {
      var command = "select * from Users where Username = @Username";
      return Connection.Query<User>(command, new { username }).Any();
    }

    // Checkin file
    public void CheckIn(string username, string projectName, string filename, byte[] fileData)
    {
      var userId = GetUserId(username);
      var projectId = GetProjectId(username, projectName);

      var command = "exec CheckIn @UserId, @ProjectId, @Filename, @FileData";
      Connection.Query(command, new { userId, projectId, filename, fileData });
    }

    // Checkout file
    public CmaFile CheckOut(string username, string projectName, string filename)
    {
      var userId = GetUserId(username);
      var projectId = GetProjectId(username, projectName);

      var command = "exec CheckOut @UserId, @ProjectId, @Filename";
      return Connection.Query<CmaFile>(command, new { userId, projectId, filename })
        .FirstOrDefault();
    }

    // Get project ID (used internally in the local repository)
    public int GetProjectId(string username, string name)
    {
      var userId = GetUserId(username);
      var command = "select top 1 Id from Projects where UserId = @UserId and Name = @Name order by Id desc";
      return Connection.Query<int>(command, new { userId, name })
        .FirstOrDefault();
    }

    // Get user ID (used internally in the local repository)
    public int GetUserId(string name)
    {
      var command = "select top 1 Id from Users where Username = @Name order by Id desc";
      return Connection.Query<int>(command, new { name })
        .FirstOrDefault();
    }

    // Get all projects for a user
    public IEnumerable<string> GetProjects(string username)
    {
      var userId = GetUserId(username);
      var command = "select Name from Projects where UserId = @UserId order by Name";
      return Connection.Query<string>(command, new { userId });
    }

    // Create a project for a user
    public int CreateProject(string username, string projectName)
    {
      var userId = GetUserId(username);
      var command = "insert into Projects(UserId, Name) values(@UserId, @ProjectName)";
      Connection.Query(command, new { userId, projectName });

      return 1; // Todo: return project id
    }

    // Get all files associated with a user and project
    public IEnumerable<CmaFile> GetFiles(string username, string project)
    {
      var userId = GetUserId(username);
      var projectId = GetProjectId(username, project);
      var command = "select a.Id, a.Name, a.FileData as Data, b.Name as ProjectName from Files a inner join Projects b on a.ProjectId = b.Id where ProjectId = @ProjectId order by Name";
      return Connection.Query<CmaFile>(command, new { userId, projectId });
    }
  }
}
